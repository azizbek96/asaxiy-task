// #tabWidth: 3
// #useTabs: true
// #singleQuote: true
// #plugins:
//   - '/usr/lib/node_modules/@prettier/plugin-php/'
// #trailingCommaPHP: true

// # prettier -w "**/*.php"

const { exec } = require('child_process');

const globalRoot= new Promise((resolve) => {
	exec('npm root -g', (err, stdout) => {
		const path = stdout.replace(/\n/g, '');
		module.paths.push(path)
		resolve(require.resolve('@prettier/plugin-php'))
	})
})

module.exports = {
	tabWidth: 3,
	plugins: [],
	useTabs: true,
	printWidth: 200,
	singleQuote: true,
	trailingCommaPHP: true,
}

globalRoot.then(modulePath=>{
	module.exports.plugins.push(modulePath)
})