    <?php

use sizeg\jwt\Jwt;
use yii\helpers\Url;
use yii\web\Response;
use yii\rest\UrlRule;
use yii\web\JsonParser;
use app\modules\admin\models\Users;
use yii\web\MultipartFormDataParser;
use app\components\JwtValidationData;

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
	'id' => 'basic',
	'name' => 'API',
	'timeZone' => 'Asia/Tashkent',
	'language' => 'uz',
	'basePath' => dirname(__DIR__),
	'bootstrap' => ['log'],
	'aliases' => [
		'@bower' => '@vendor/bower-asset',
		'@npm' => '@vendor/npm-asset',
	],
	'modules' => [
		'api' => [
			'class' => 'app\modules\api\Module',
			'as access' => [
				'class' => 'yii\filters\AccessControl',
				'rules' => [
					[
						'allow' => true,
						'roles' => ['?'],
					],
				],
			],
		],
	],
	'components' => [
		'authManager' => [
			'class' => 'yii\rbac\DbManager',
		],
		'request' => [
			'cookieValidationKey' => 'api',
			'baseUrl' => '/',
			'parsers' => [
				'application/json' => JsonParser::class,
				'multipart/form-data' => MultipartFormDataParser::class,
			],
		],
		'response' => [
			'charset' => 'UTF-8',
			'format' => Response::FORMAT_JSON,
			'on beforeSend' => function ($event) {
				header('Access-Control-Allow-Origin: *');
			},
		],
		'cache' => [
			'class' => 'yii\caching\FileCache',
		],
		'user' => [
			'identityClass' => Users::class,
			'loginUrl' => ['/api/auth/login'],
			'authTimeout' => 3600 * 24,
			'enableAutoLogin' => true,
			'enableSession' => false,
		],
		'errorHandler' => [
			'errorAction' => 'site/error',
		],
		'jwt' => [
			'class' => Jwt::class,
			'key' => 'f2FgtDa2PefDCBMfdpywEue5mHekRQA2lh12Bt/B0Ziuy',
			'jwtValidationData' => JwtValidationData::class,
		],
        'i18n' => [
            'translations' => [
                'app*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/messages',
                    //'sourceLanguage' => 'en-US',
                    'fileMap' => [
                        'app' => 'app.php',
                        'app/error' => 'error.php',
                    ],
                ],
            ],
        ],
        'mailer' => [
			'class' => 'yii\swiftmailer\Mailer',
			'useFileTransport' => true,
		],
		'log' => [
			'traceLevel' => YII_DEBUG ? 3 : 0,
			'targets' => [
				[
					'class' => 'yii\log\FileTarget',
					'levels' => ['error', 'warning'],
				],
			],
		],
		'db' => $db,

		'urlManager' => [
			'enablePrettyUrl' => true,
			'showScriptName' => false,
			'baseUrl' => '/',
			'rules' => [
				[
					'class' => UrlRule::class,
					'controller' => [
                        'api/auth',
                        'api/users',
                        'api/signup',
                        'api/users-info',
                        'api/candidate-note',
                    ],
					'extraPatterns' => [
						'GET index' => 'index',
                        'POST application' => 'application',
                        'POST message' => 'message',
                        'OPTIONS application' => 'application',
                        'OPTIONS message' => 'message',

						'GET login' => 'login',
						'POST login' => 'login',
						'OPTIONS login' => 'login',

                        'POST create' => 'create',
                        'GET create' => 'create',
                        'OPTIONS create' => 'create',

                        'DELETE delete' => 'delete',
                        'OPTIONS delete' => 'delete',

                        'GET view' => 'view',
					],
				],
				'<controller:\w+>/<id:\d+>' => '<controller>/view',
				'<controller:\w+>/<action:\w+>' => '<controller>/<action>',
				'<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
			],
		],
		'formatter' => [
			'class' => 'yii\i18n\Formatter',
			'dateFormat' => 'd.m.Y',
			'datetimeFormat' => 'd.m.Y H:i:s',
			'timeFormat' => 'H:i:s',
			'nullDisplay' => '',
			'thousandSeparator' => '',
			'decimalSeparator' => '.',
		],
	],
    'as beforeRequest' => [
        'class' => 'app\components\SetLanguages',
    ],
	'params' => $params,
];

if (YII_ENV_DEV) {
	// configuration adjustments for 'dev' environment
	$config['bootstrap'][] = 'debug';
	$config['modules']['debug'] = [
		'class' => 'yii\debug\Module',
		'allowedIPs' => ['127.0.0.1', '::1', '192.168.1.16'],
		// uncomment the following to add your IP if you are not connecting from localhost.
	];

	$config['bootstrap'][] = 'gii';
	$config['modules']['gii'] = [
		'class' => 'yii\gii\Module',
		// uncomment the following to add your IP if you are not connecting from localhost.
		//'allowedIPs' => ['127.0.0.1', '::1'],
	];
}

return $config;
