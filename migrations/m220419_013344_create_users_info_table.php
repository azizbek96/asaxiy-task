<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%users_info}}`.
 */
class m220419_013344_create_users_info_table extends Migration
{
	/**
	 * {@inheritdoc}
	 */
	public function safeUp()
	{
		$this->createTable('{{%users_info}}', [
			'id' => $this->primaryKey(),
			'first_name' => $this->string(),
			'last_name' => $this->string(),
            'address' => $this->text(),
            'country_of_origin' => $this->string(),
            'phone_number' => $this->string(20),
            'email' => $this->string(60),
            'age' => $this->integer(),
            'hired' => $this->boolean(),
            'status' => $this->smallInteger()->defaultValue(1),
            'type' => $this->integer(),
            'created_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_at' => $this->integer(),
            'updated_by' => $this->integer(),
		]);

	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
	{
		$this->dropTable('{{%users_info}}');
	}
}
