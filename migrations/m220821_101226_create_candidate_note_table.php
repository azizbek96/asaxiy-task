<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%candidate_note}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%users}}`
 * - `{{%users}}`
 */
class m220821_101226_create_candidate_note_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%candidate_note}}', [
            'id' => $this->primaryKey(),
            'candidate_id' => $this->integer(),
            'user_id' => $this->integer(),
            'note' => $this->text(),
            'date' => $this->dateTime(),'status' => $this->smallInteger()->defaultValue(1),
            'type' => $this->integer(),
            'created_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_at' => $this->integer(),
            'updated_by' => $this->integer(),
        ]);

        // creates index for column `candidate_id`
        $this->createIndex(
            '{{%idx-candidate_note-candidate_id}}',
            '{{%candidate_note}}',
            'candidate_id'
        );

        // add foreign key for table `{{%users}}`
        $this->addForeignKey(
            '{{%fk-candidate_note-candidate_id}}',
            '{{%candidate_note}}',
            'candidate_id',
            '{{%users}}',
            'id',
            'CASCADE'
        );

        // creates index for column `user_id`
        $this->createIndex(
            '{{%idx-candidate_note-user_id}}',
            '{{%candidate_note}}',
            'user_id'
        );

        // add foreign key for table `{{%users}}`
        $this->addForeignKey(
            '{{%fk-candidate_note-user_id}}',
            '{{%candidate_note}}',
            'user_id',
            '{{%users}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%users}}`
        $this->dropForeignKey(
            '{{%fk-candidate_note-candidate_id}}',
            '{{%candidate_note}}'
        );

        // drops index for column `candidate_id`
        $this->dropIndex(
            '{{%idx-candidate_note-candidate_id}}',
            '{{%candidate_note}}'
        );

        // drops foreign key for table `{{%users}}`
        $this->dropForeignKey(
            '{{%fk-candidate_note-user_id}}',
            '{{%candidate_note}}'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            '{{%idx-candidate_note-user_id}}',
            '{{%candidate_note}}'
        );

        $this->dropTable('{{%candidate_note}}');
    }
}
