<?php

use yii\db\Migration;

/**
 * Class m220821_102000_insert_rbac_permissions
 */
class m220821_102000_insert_rbac_permissions extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        return true;
//        $this->insert('auth_item', ['name' => 'auth-assignment', 'type' => '3', 'description' => '']);
//        $this->insert('auth_item', ['name' => 'auth-assignment/index', 'type' => '2', 'description' => '']);
//        $this->insert('auth_item', ['name' => 'auth-assignment/delete', 'type' => '2', 'description' => '']);
//        $this->insert('auth_item', ['name' => 'auth-assignment/update', 'type' => '2', 'description' => '']);
//        $this->insert('auth_item', ['name' => 'auth-assignment/create', 'type' => '2', 'description' => '']);
//        $this->insert('auth_item', ['name' => 'auth-assignment/view', 'type' => '2', 'description' => '']);
//
//        $this->insert('auth_item', ['name' => 'auth-item', 'type' => '3', 'description' => '']);
//        $this->insert('auth_item', ['name' => 'auth-item/index', 'type' => '2', 'description' => '']);
//        $this->insert('auth_item', ['name' => 'auth-item/create', 'type' => '2', 'description' => '']);
//        $this->insert('auth_item', ['name' => 'auth-item/delete', 'type' => '2', 'description' => '']);
//        $this->insert('auth_item', ['name' => 'auth-item/view', 'type' => '2', 'description' => '']);
//        $this->insert('auth_item', ['name' => 'auth-item/update', 'type' => '2', 'description' => '']);
//
//
//        $this->insert('auth_item', ['name' => 'auth-item-child', 'type' => '3', 'description' => '']);
//        $this->insert('auth_item', ['name' => 'auth-item-child/index', 'type' => '2', 'description' => '']);
//        $this->insert('auth_item', ['name' => 'auth-item-child/update', 'type' => '2', 'description' => '']);
//        $this->insert('auth_item', ['name' => 'auth-item-child/create', 'type' => '2', 'description' => '']);
//        $this->insert('auth_item', ['name' => 'auth-item-child/delete', 'type' => '2', 'description' => '']);
//        $this->insert('auth_item', ['name' => 'auth-item-child/view', 'type' => '2', 'description' => '']);
//
//        $this->insert('auth_item', ['name' => 'users', 'type' => '3', 'description' => '']);
//        $this->insert('auth_item', ['name' => 'users/index', 'type' => '2', 'description' => '']);
//        $this->insert('auth_item', ['name' => 'users/create', 'type' => '2', 'description' => '']);
//        $this->insert('auth_item', ['name' => 'users/update', 'type' => '2', 'description' => '']);
//        $this->insert('auth_item', ['name' => 'users/view', 'type' => '2', 'description' => '']);
//        $this->insert('auth_item', ['name' => 'users/delete', 'type' => '2', 'description' => '']);
//
//        // roles
//        $this->insert('auth_item', ['name' => 'candidate', 'type' => '1', 'description' => '']);
//        $this->insert('auth_item', ['name' => 'user', 'type' => '1', 'description' => '']);
//        $this->insert('auth_item', ['name' => 'super-admin', 'type' => '1', 'description' => '']);
//
//        // Auth-item-child inserts
//        $this->insert('auth_item_child', ['parent' => 'super-admin', 'child' => 'auth-assignment/index']);
//        $this->insert('auth_item_child', ['parent' => 'super-admin', 'child' => 'auth-assignment/delete']);
//        $this->insert('auth_item_child', ['parent' => 'super-admin', 'child' => 'auth-assignment/update']);
//        $this->insert('auth_item_child', ['parent' => 'super-admin', 'child' => 'auth-assignment/create']);
//        $this->insert('auth_item_child', ['parent' => 'super-admin', 'child' => 'auth-assignment/view']);
//
//        $this->insert('auth_item_child', ['parent' => 'super-admin', 'child' => 'auth-item/index']);
//        $this->insert('auth_item_child', ['parent' => 'super-admin', 'child' => 'auth-item/create']);
//        $this->insert('auth_item_child', ['parent' => 'super-admin', 'child' => 'auth-item/delete']);
//        $this->insert('auth_item_child', ['parent' => 'super-admin', 'child' => 'auth-item/view']);
//        $this->insert('auth_item_child', ['parent' => 'super-admin', 'child' => 'auth-item/update']);
//
//        $this->insert('auth_item_child', ['parent' => 'super-admin', 'child' => 'auth-item-child/index']);
//        $this->insert('auth_item_child', ['parent' => 'super-admin', 'child' => 'auth-item-child/delete']);
//        $this->insert('auth_item_child', ['parent' => 'super-admin', 'child' => 'auth-item-child/view']);
//        $this->insert('auth_item_child', ['parent' => 'super-admin', 'child' => 'auth-item-child/update']);
//        $this->insert('auth_item_child', ['parent' => 'super-admin', 'child' => 'auth-item-child/create']);
//
//        $this->insert('auth_item_child', ['parent' => 'super-admin', 'child' => 'users/index']);
//        $this->insert('auth_item_child', ['parent' => 'super-admin', 'child' => 'users/delete']);
//        $this->insert('auth_item_child', ['parent' => 'super-admin', 'child' => 'users/view']);
//        $this->insert('auth_item_child', ['parent' => 'super-admin', 'child' => 'users/update']);
//        $this->insert('auth_item_child', ['parent' => 'super-admin', 'child' => 'users/create']);
//
//        $this->insert('auth_item', ['name' => 'users', 'type' => '3', 'description' => '']);
//        $this->insert('auth_item', ['name' => 'users/index', 'type' => '2', 'description' => '']);
//        $this->insert('auth_item', ['name' => 'users/create', 'type' => '2', 'description' => '']);
//        $this->insert('auth_item', ['name' => 'users/update', 'type' => '2', 'description' => '']);
//        $this->insert('auth_item', ['name' => 'users/view', 'type' => '2', 'description' => '']);
//        $this->insert('auth_item', ['name' => 'users/delete', 'type' => '2', 'description' => '']);
//
//        $this->insert('auth_assignment', ['item_name' => 'super-admin', 'user_id' => '1']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return true;
    }

}
