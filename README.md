<p align="center">
    <h1 align="center">Asaxiy task </h1>
    <br>
</p>


CONFIGURATION
-------------

### Database

Edit the file `config/db.php` with real data, for example:

```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'pgsql:host=localhost;port=5432;dbname=asaxiy_task',
    'username' => 'root',
    'password' => '1234',
    'charset' => 'utf8',
];
```

* composer update 
~~~
composer update
~~~
* Migratsion yurgazib olinsin
~~~
php yii migrate -p=@yii/rbac/migrations
php yii migrate
~~~

### Nomzod

1) Nomzon ro'yhatdan o'tadi  `http://localhost:8080/api/signup/login` 

```js
{
    "username": "nomzod",
    "password_hash": "12345678",
    "first_name": "Azizbek",
    "last_name": "Ismoilov",
    "address": "Farg'ona viloyati",
    "country_of_origin": "Maqsadim yaxshi kasbdoshlar topish",
    "phone_number": "+998936443404",
    "email": "ai1996yil@gmail.com",
    "age": "26"
}
```
2. Nomzon ariza tashlash  `http://localhost:8080/api/candidate-note/application`

```
```
3. Nomzon arizani yangiliash  `http://localhost:8080/api/candidate-note/update?id=7`

```js
{
    "candidate_id": 7,//nomzod id si
    "note": "sizlarda bosh ish o'rin boormi",
    "date": "2022-08-23 11:11:11"
}
```


### Admin

1) Admin ro'yhatdan o'tishi  `http://localhost:8080/api/auth/login`
    __________________**Body**
```js
{
    "username": "admin",
    "password": "123456"
}
```
2) Admin ro'yhatdan o'tishi  `http://localhost:8080/api/candidate-note/message`
    __________________**Body**
```js
{
    "candidate_id": 7,
    "note": "Sizi biz intervyuga taklif qilamiz",
    "date": "2022-08-23 11:11:11"
}
```

4. Nomzonni GET — id beriladi, va shu id dagi nomzod maʼlumotlarini olish;  `http://localhost:8080/api/users-info/view?id=7`
   _______________ ***~Response~***
```js
{
    "code": 200,
    "message": "",
    "data": {
        "first_name": "Azizbek",
        "last_name": "Ismoilov",
        "address": "Farg'ona viloyati",
        "country_of_origin": "asdfg",
        "phone_number": "+998936443404",
        "email": "azizbek1@gmail.com",
        "age": null,
        "status": 1,
        "type": 6
    }
}
```
 5)  Nomzod uchun statuslar 
    `STATUS`
     ```
     STATUS_ACTIVE = 1; // yangi intervyuga kelgan
     STATUS_INACTIVE = 2; // faol emas
     STATUS_SAVED = 3;
     STATUS_DELETED = 4; // o'chirilgan
     STATUS_APPLICATION = 5;// ariza berilgan
     STATUS_TALK = 6;// interevyyu belgilangan
     STATUS_ACCEPT = 7; //qabul qilingan
     STATUS_NO_ACCEPT = 8; //qabul qilinmagan
     STATUS_ADMIN = 10; // admin
     ```
6) Nomzod statusni o'zgartirish ``http://localhost:8080/api/users/candidate-status``
    `STATUS`
     ```js
    {
    "candidate_id": 7,
    "status" : 7
    }
     ```