<?php

namespace app\controllers;

use Yii;
use app\components\CorsCustom;
use app\modules\admin\models\Users;
use app\modules\api\models\BaseModel;
use Lcobucci\JWT\Parser;
use sizeg\jwt\JwtHttpBearerAuth;
use yii\helpers\ArrayHelper;
use yii\rest\Controller;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;

class BaseController extends Controller
{
	/**
	 * @return array
	 */
	public function behaviors(): array
	{
		$behaviors = parent::behaviors();

		$auth = $behaviors['authenticator'] = [
			'class' => JwtHttpBearerAuth::class,
		];

		unset($behaviors['authenticator']);

		$behaviors['corsFilter'] = [
			'class' => CorsCustom::class,
		];

		$behaviors['authenticator'] = $auth;

		$behaviors['authenticator']['except'] = ['login', 'refresh-token', 'options'];

		return $behaviors;
	}

    /**
     * @param $action
     * @return bool
     * @throws BadRequestHttpException
     */
	public function beforeAction($action): bool
	{
		$this->enableCsrfValidation = false;
		$headers = Yii::$app->request->headers;
		$pattern = "/^Bearer\s+(.*?)$/";
		preg_match($pattern, $headers->get('authorization'), $matches);
		if (isset($matches[1])) {
			if (!isset(Yii::$app->user->id)) {
				$token = (new Parser())->parse($matches[1]);
				$userId = $token->getClaim('uid');
				if ($userId) {
					$user = Users::findOne(['id' => $userId]);
					if ($user !== null) {
						Yii::$app->user->setIdentity($user);
					}
				}
			}
		}
//		$controller = Yii::$app->controller->id;
//		switch ($controller) {
//			case 'appointment':
//				$slug = Yii::$app->request->get('slug');
//				$isKey = false;
//				if (!empty($slug)) {
//					if (array_key_exists($slug, AppointmentRepository::geDocumentByLabel())) {
//						$isKey = true;
//						$this->slug = $slug;
//					}
//				}
//				if (!$isKey) {
//					throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
//				}
//				if (Yii::$app->authManager->getPermission(Yii::$app->controller->id . '/' . Yii::$app->controller->action->id)) {
//					if (!P::can(Yii::$app->controller->id . '/' . $this->slug . '/' . Yii::$app->controller->action->id)) {
//						throw new ForbiddenHttpException(Yii::t('app', 'Access denied'));
//					}
//				}
//				break;
//			default:
//				return parent::beforeAction($action);
//		}
//		        if (Yii::$app->authManager->getPermission(Yii::$app->controller->id . "/" . Yii::$app->controller->action->id)) {
//		            if (P::can(Yii::$app->controller->id . "/" . Yii::$app->controller->action->id)) {
//		                throw new ForbiddenHttpException(Yii::t('app', 'Access denied'));
//		            }
//		        }else{
//		            throw new ForbiddenHttpException(Yii::t('app', 'Access denied'));
//		        }
		return parent::beforeAction($action);
	}

	/**
	 * @param $data
	 * @param $other
	 * @param string $message
	 * @return array
	 */
	public function success($data = [], $other = [], string $message = ''): array
	{
		return ArrayHelper::merge($other, [
			'code' => BaseModel::CODE_SUCCESS,
			'message' => $message ?? Yii::t('app', 'Success'),
			'data' => $data,
		]);
	}

	/**
	 * @param array $data
	 * @param string $message
	 * @param int $code
	 * @return array
	 */
	public function error(array $data = [], string $message = '', int $code = BaseModel::CODE_ERROR): array
	{
		return [
			'data' => $data,
			'code' => $code,
			'message' => $message ?? Yii::t('app', 'Error'),
		];
	}
}
