<?php

namespace app\modules\api\controllers;

use app\modules\admin\repositories\UsersInfoRopository;

class UsersInfoController extends \app\controllers\BaseController
{
    private $ropository;
    public function __construct($id, $module,UsersInfoRopository $ropository, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->ropository =$ropository;
    }

    public function actionView($id)
    {
        $candidate  = $this->ropository->getUserInfo($id);
        return $candidate['status']?$this->success($candidate['data']): $this->error();
    }
}