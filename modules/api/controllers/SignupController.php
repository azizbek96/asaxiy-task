<?php

namespace app\modules\api\controllers;

use app\modules\admin\entities\User\UsersInfo;
use app\modules\admin\helpers\JWTHelpers;
use app\modules\admin\services\auth\SingupService;
use app\modules\admin\forms\auth\SignupForm;
use app\controllers\BaseController;
use Yii;
use yii\web\ServerErrorHttpException;

class SignupController extends BaseController
{
    private SingupService $service;
    private JWTHelpers $jwtHelpers;
    public function __construct($id, $module, SingupService $service, JWTHelpers $jwtHelpers, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;
        $this->jwtHelpers = $jwtHelpers;
    }

    /**
     * @return array
     * @return string
     * @throws ServerErrorHttpException
     * @throws \yii\base\Exception
     */
    public function actionLogin()
    {
        $form = new SignupForm();
        if ($form->load(Yii::$app->request->post(),'') && $form->validate() ) {
            $user =  $this->service->signup($form);
            if (!empty($user)) {
                $token = $this->jwtHelpers->generateJwt($user);
                $this->jwtHelpers->generateRefreshToken($user);
                return $this->success([
                    'user' => [
                        'username' => $user->username,
                        'roles' => Yii::$app->authManager->getPermissionsByUser($user->id),
                    ],
                    'token' => (string) $token,
                ]);
            } else {
                throw new ServerErrorHttpException('Candidate  not saved');
            }
        } else {
            return $this->error($form->getFirstErrors());
        }
    }

}