<?php

namespace app\modules\api\controllers;

use app\modules\directory\models\DirectoryType;
use app\modules\api\models\BaseModel;
use Yii;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\db\StaleObjectException;
use app\controllers\BaseController;
use app\modules\admin\models\AuthItem;
use app\modules\admin\models\AuthItemChild;
use app\modules\admin\models\AuthItemSearch;
use app\modules\admin\models\AuthAssignmentSearch;
use app\modules\api\responses\AuthItemResponse;

class AuthItemController extends BaseController
{
    public string $type = 'role';

    /**
     * @return array
     */
    public function actionIndexRole($page = 1)
    {
        if (Yii::$app->request->isGet) {
            $searchModel = new AuthItemSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $page);
            $totalCount = $dataProvider->totalCount;
            $pagination = $dataProvider->pagination;

            $role_type = [];

            foreach (AuthItem::getRoleType() as $key => $rt) {
                $role_type[] = [
                    'label' => $rt,
                    'value' => $key,
                ];
            }

            return $this->success($dataProvider, [
                'role_type' => $role_type,
                'pagination' => [
                    'totalSize' => $totalCount,
                    'page' => $pagination->page + 1,
                    'sizePerPage' => $pagination->pageSize,
                    'pageCount' => ceil($totalCount / $pagination->pageSize),
                ],
            ]);
        } else {
            return $this->error();
        }
    }

    public function actionSearch($role = false)
    {
        $params = $this->request->queryParams;

        $query = AuthItem::find()
            ->select([
                'value' => 'name',
                'label' => '(CASE WHEN name_for_user ISNULL THEN name ELSE name_for_user END)',
            ])
            ->where([
                'type' => $role ? BaseModel::USER_ROLE_TYPE_ROLE : BaseModel::USER_ROLE_TYPE_PERMISSION,
            ]);

        $query->andWhere(['~*', 'name', $params['name']]);

        if (!empty($params['user_id'])) {
            $userRoles = array_map(function ($item) {
                return $item['value'];
            }, AuthAssignmentSearch::getUserRolesForSelect($params['user_id']));

            $query->andWhere(['NOT IN', 'name', $userRoles]);
        }

        return $query->asArray()->all();
    }

    /**
     * @return array
     */
    public function actionIndexPermission()
    {
        $searchModel = new AuthItemSearch();
        $dataProvider = $searchModel->permission()->search(Yii::$app->request->queryParams);
        $totalCount = $dataProvider->totalCount;
        $pagination = $dataProvider->pagination;
        return $this->success($dataProvider, [
            'pagination' => [
                'totalSize' => $totalCount,
                'page' => $pagination->page + 1,
                'sizePerPage' => $pagination->pageSize,
                'pageCount' => ceil($totalCount / $pagination->pageSize),
            ],
        ]);
    }

    /**
     * @param $id
     * @return array
     */
    public function actionView($id)
    {
        if (Yii::$app->request->isGet) {
            $model = $this->findModel($id);
            return $this->success($model);
        } else {
            return $this->error();
        }
    }

    public function actionCreateRole()
    {
        if (Yii::$app->request->isGet) {
            $role_type = AuthItem::getRoleType();
            return $this->success([
                'role_type' => $role_type,
            ]);
        }
        if (Yii::$app->request->isPost) {
            $model = new AuthItem();
            $data = Yii::$app->request->post();
            $model->setAttributes([
                'type' => 1,
                'created_at' => time(),
                'updated_at' => time(),
                'name' => $data['name'],
                'name_for_user' => $data['name_for_user'],
                'description' => $data['description'] ?? null,
            ]);
            if ($model->save()) {
                return $this->success(AuthItemResponse::create($model));
            } else {
                return $this->error(['errors' => $model->getErrors()]);
            }
        }
        return $this->error();
    }

    /**
     * @param $name
     * @return array
     */
    public function actionUpdateRole($name = null): array
    {
        $model = $this->findModel($name);
        if (Yii::$app->request->isGet) {
            return $this->success([
                'roleName' => $name,
                'role_type' => AuthItem::getRoleType(),
                'role' => AuthItemResponse::update($model),
            ]);
        }
        if (Yii::$app->request->isPut) {
            if (!empty($model['code']) && $model['code'] == BaseModel::CODE_ERROR) {
                return $model;
            } else {
                $data = Yii::$app->request->post();
                $model->setAttributes([
                    'updated_at' => time(),
                    'name' => $data['name'],
                    'type' => BaseModel::USER_ROLE_TYPE_ROLE,
                    'name_for_user' => $data['name_for_user'],
                ]);
                if ($model->save()) {
                    return $this->success(AuthItemResponse::update($model, $data['old_name']));
                } else {
                    return $this->error(['errors' => $model->errors]);
                }
            }
        }
        return $this->error();
    }

    /**
     * @return array
     * @throws Exception
     */
    public function actionCreatePermission()
    {
        $model = new AuthItem();
        if (Yii::$app->request->isGet) {
            return $this->success([
                'categories' => $model->getCategory(true),
            ]);
        }
        if (Yii::$app->request->isPost) {
            $data = Yii::$app->request->post();
            if (!empty($data['permissions'])) {
                $permissions = [];
                foreach ($data['permissions'] as $item) {
                    $dataAI = [];
                    $m = new AuthItem();
                    $dataAI['AuthItem']['type'] = BaseModel::USER_ROLE_TYPE_PERMISSION;
                    $dataAI['AuthItem']['category'] = $data['category'];
                    $dataAI['AuthItem']['description'] = $item['description'];
                    $dataAI['AuthItem']['name_for_user'] = $item['name_for_user'] ?? null;
                    $dataAI['AuthItem']['name'] = $data['name'] . '/' . $item['name'];

                    if ($m->load($dataAI) && $m->save()) {
                        $auth = Yii::$app->authManager;
                        $role = $auth->getRole($m->category);
                        $permission = $auth->getPermission($m->name);
                        $auth->addChild($role, $permission);
                        $permissions[] = AuthItemResponse::create($m);
                    } else {
                        return $this->error(['errors' => $m->errors]);
                    }
                }
                return $this->success($permissions);
            }
        }
        return $this->error();
    }

    /**
     * @param $id
     * @return array
     * @throws Exception
     */
    public function actionUpdatePermission($name)
    {
        $model = $this->findModel($name);
        $model->type = 2;
        if (Yii::$app->request->isGet) {
            return $this->success([
                'values' => AuthItemResponse::update($model),
                'categories' => $model->getCategory(true),
            ]);
        } elseif (Yii::$app->request->isPut) {
            $data = Yii::$app->request->post();
            $data['type'] = 2;
            if ($model->load($data, '') && $model->save()) {
                $auth = Yii::$app->authManager;
                $this->deletePermission($model->name);
                $role = $auth->getRole($model->category);
                $permission = $auth->getPermission($model->name);
                $auth->addChild($role, $permission);
                return $this->success();
            } else {
                return $this->error(['errors' => $model->errors]);
            }
        }
        return $this->error();
    }

    /**
     * @param $id
     * @return array
     * @throws StaleObjectException
     */
    public function actionDelete($name)
    {
        if (Yii::$app->request->isDelete) {
            $model = $this->findModel($name);
            if (!empty($model)) {
                if ($model->delete()) {
                    return $this->success();
                } else {
                    return $this->error(['errors' => $model->errors]);
                }
            } else {
                return $this->error([], Yii::t('app', 'Not found'));
            }
        }
        return $this->error();
    }

    /**
     * @param $perm
     * @return void
     * @throws StaleObjectException
     */
    protected function deletePermission($perm)
    {
        $model = AuthItemChild::find()
            ->where(['child' => $perm])
            ->one();
        if (!empty($model)) {
            $model->delete();
        }
    }

    public function actionMenu()
    {
        if (Yii::$app->request->isGet) {
            $menu = [
                [
                    'label' =>Yii::t('app','Structure organization'),
                    'icon' => 'bx bx-cog',
                    'content' => [
                        [
                            'to' => '/users',
                            'label' => Yii::t('app', 'Users'),
                            'icon' => 'fas fa-users',
                            'visible' => true,
                        ],
                        [
                            'to' => '/user-roles',
                            'label' => Yii::t('app', 'User Roles'),
                            'icon' => 'bx bx-key',
                            'visible' => true,
                        ],
                        [
                            'to' => '/user-permissions',
                            'icon' => 'bx bx-dialpad-alt',
                            'label' => Yii::t('app', 'User Permissions'),
                            'visible' => true,
                        ],
                    ],
                ],
            ];

            foreach ($menu as $menuKey => $menuItem) {
                if (!empty($menuItem['content'])) {
                    foreach ($menuItem['content'] as $key => $item) {
                        if ($item['visible'] == true) {
                            unset($menu[$menuKey]['content'][$key]['visible']);
                        } else {
                            unset($menu[$menuKey]['content'][$key]);
                        }
                    }
                }
            }

            return $this->success($menu);
        }

        return $this->error([]);
    }

    /**
     * @param string $name
     * @return AuthItem|array|ActiveRecord
     */
    private function findModel(string $name)
    {
        $model = AuthItem::find()
            ->select(['name', 'data', 'category', 'role_type', 'rule_name', 'description', 'name_for_user'])
            ->where(['name' => $name])
            ->one();
        if ($model !== null) {
            return $model;
        }
        return $this->error([], Yii::t('app', 'Not found'));
    }
}
