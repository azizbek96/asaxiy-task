<?php

namespace app\modules\api\controllers;

use app\components\Constanta;
use app\modules\admin\entities\User\Users;
use app\modules\admin\forms\CandidateStatusForm;
use app\modules\admin\repositories\UsersInfoRopository;
use yii\web\MethodNotAllowedHttpException;
use app\controllers\BaseController;
use Yii;

/**
 * Class UserController
 */
class UsersController extends BaseController
{
    public function actionCandidateStatus(){
        if (Yii::$app->request->isPut){
            $form = new CandidateStatusForm();
            $response = [];
            $response['status'] = false;
            if ($form->load(Yii::$app->request->post(),'') && $form->validate()){
                $response['status'] = true;
                $form->hired = Constanta::HIRED_FALSE;
                if ($form->status == Constanta::STATUS_ACCEPT)
                    $form->hired= Constanta::HIRED_TRUE;
                $user = Users::changeStatus($form->candidate_id, $form->status);
                $userInfo = UsersInfoRopository::getUserInfoModel($user->user_info_id);
                $userInfo->hired = $form->hired;
                if ($user->save() && $userInfo->save()){
                    $response['status'] = true;
                    $response['success'] = Yii::t('app','status o\'zgartirildi ');
                }
            }
            return $response['status'] ? $this->success($response['success']) : $this->error($form->getErrors());
        }
        throw new MethodNotAllowedHttpException(Yii::t('app', 'Method Not Allowed'));
    }
}
