<?php

namespace app\modules\api\controllers;

use app\controllers\BaseController;
use app\modules\admin\entities\manage\CandidateNote;
use app\modules\admin\forms\CandidateNoteForm;
use app\modules\admin\repositories\CandidateNoteRepository;
use app\modules\admin\repositories\UsersRepository;
use Yii;
use yii\web\MethodNotAllowedHttpException;

class CandidateNoteController extends BaseController
{

    public function actionMessage()
    {
        $form = new CandidateNoteForm();
        if (Yii::$app->request->isPost){
            if ($form->load(Yii::$app->request->post(),'') && $form->validate() ) {
               $id = Yii::$app->user->id;
                   (new UsersRepository)->message($form->candidate_id);
                   $candidateNew = CandidateNote::create(
                       $form->note,
                       $form->date,
                       $form->candidate_id,
                       $id
                   );
                   if ($candidateNew->save()){
                      return $this->success(Yii::t('app','{candidate_id} Ush bu nomzodga intervyu belgilandi ',['candidate_id' => $form->candidate_id]));
                   }
               }
           }else{
           return $this->error($form->errors);
       }
        throw new MethodNotAllowedHttpException(Yii::t('app', 'Method Not Allowed'));

    }

    public function actionApplication()
    {
        if (Yii::$app->request->isPost){
            $response = [];
            $response['status'] = false;
            $id = Yii::$app->user->id;
            $candidate = CandidateNoteRepository::get($id);
            if ($candidate){
                $response['warning'] = Yii::t('app','Avval ariza topshirgansiz!!! ');
            }else{
                $date = date('Y-m-d H:i:s');
                $response['status'] = true;
                (new UsersRepository)->aplication($id);
                $candidateNew = CandidateNote::create('', $date, $id);
                $candidateNew->save();
                $response['success'] = Yii::t('app','Arizangiz qabul qilindi ');
            }
            return $response['status'] ? $this->success($response['success']) : $this->success($response['warning']);
        }
        throw new MethodNotAllowedHttpException(Yii::t('app', 'Method Not Allowed'));

    }

    public function actionUpdate($id)
    {

        $candidate = CandidateNoteRepository::get($id);
        if (Yii::$app->request->isGet){
            return $this->success($candidate);
        }
        if (Yii::$app->request->isPut){
            $response = [];
            $response['status'] = false;
            $form = new CandidateNoteForm();
            if ($form->load(Yii::$app->request->post(),'') && $form->validate()){
                $response['status'] = true;
                (new UsersRepository)->aplication($id);
                $candidateNew = CandidateNote::create(
                    $form->note, $form->date, $form->candidate_id);
                $candidateNew->save();
                $response['success'] = Yii::t('app','Arizangiz qabul qilindi ');
            }
            return $response['status'] ? $this->success($response['success']) : $this->error($form->getErrors());
        }
        throw new MethodNotAllowedHttpException(Yii::t('app', 'Method Not Allowed'));
    }

    public function actionDelete($id)
    {
        if (Yii::$app->request->isDelete){
            $response = [];
            $response['status'] = false;
            $response['success'] = Yii::t('app','Arizangiz o\'chirib bo\'lmadi ');
            $candidateDelete = CandidateNoteRepository::get($id);
            if ($candidateDelete && $candidateDelete->delete()){
                $response['status'] = true;
                $response['success'] = Yii::t('app','Arizangiz o\'chirib tashlandi ');
            }
            return $response['status'] ? $this->success($response['success']) : $this->error($response['success']);
        }
        throw new MethodNotAllowedHttpException(Yii::t('app', 'Method Not Allowed'));
    }
}