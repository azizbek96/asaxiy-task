<?php

namespace app\modules\api\controllers;

use app\modules\admin\entities\User\UserRefreshTokens;
use app\modules\admin\helpers\JWTHelpers;
use app\modules\admin\services\auth\AuthService;
use app\modules\admin\forms\auth\LoginForm;
use yii\base\Exception;
use yii\web\ServerErrorHttpException;
use app\controllers\BaseController;
use app\modules\admin\models\Users;
use Yii;

/**
 * Default controller for the `api` module
 */
class AuthController extends BaseController
{
    private AuthService $service;
    private JWTHelpers $jwtHelpers;

    public function __construct($id, $module,AuthService $service, JWTHelpers $jwtHelpers,$config = [])
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;
        $this->jwtHelpers = $jwtHelpers;
    }

    /**
     * @return array
     * @throws ServerErrorHttpException
     * @throws Exception
     */
	public function actionLogin(): array
    {
        $form = new LoginForm();
        $data = Yii::$app->request->post();
        if ($form->load(Yii::$app->request->post(), '') && $form->validate()) {
			$user = $this->service->auth($form);
			if (!empty($user)) {
				$token = $this->jwtHelpers->generateJwt($user);
                $this->jwtHelpers->generateRefreshToken($user);
				return $this->success([
					'user' => [
						'username' => $user->username,
						'roles' => Yii::$app->authManager->getPermissionsByUser($user->id),
					],
					'token' => (string) $token,
				]);
			} else {
				throw new ServerErrorHttpException('User not found');
			}
		} else {
			return $this->error($form->getFirstErrors());
		}
	}
    public function actionRefreshToken()
	{
		$refreshToken = Yii::$app->request->cookies->getValue('refresh-token', false);

		if (!$refreshToken) {
			return new \yii\web\UnauthorizedHttpException('No refresh token found.');
		}

		$userRefreshToken = UserRefreshTokens::findOne([
			'token' => $refreshToken,
		]);

		if (Yii::$app->request->isPost) {
			// Getting new JWT after it has expired
			if (!$userRefreshToken) {
				return new \yii\web\UnauthorizedHttpException('The refresh token no longer exists.');
			}

			$user = Users::find() //adapt this to your needs
				->where(['id' => $userRefreshToken->user_id])
				->andWhere(['not', ['status' => Users::STATUS_INACTIVE]])
				->one();
			if (!$user) {
				$userRefreshToken->delete();
				return new \yii\web\UnauthorizedHttpException('The user is inactive.');
			}

			$token = $this->generateJwt($user);

			return [
				'status' => 'ok',
				'token' => (string) $token,
			];
		} elseif (Yii::$app->request->isDelete) {
			// Logging out
			if ($userRefreshToken && !$userRefreshToken->delete()) {
				return new ServerErrorHttpException('Failed to delete the refresh token.');
			}

			return ['status' => 'ok'];
		} else {
			return new \yii\web\UnauthorizedHttpException('The user is inactive.');
		}
	}
    public function actionSignUp(){

    }
}
