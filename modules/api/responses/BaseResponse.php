<?php
namespace app\modules\api\responses;

class BaseResponse
{
	public int $id = 0;
	public string $name = '';
	public int $type = 0;
	public int $status = 0;
}
