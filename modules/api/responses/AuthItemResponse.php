<?php

namespace app\modules\api\responses;

use app\modules\admin\models\AuthItem;
use app\modules\api\models\BaseModel;
use stdClass as StdClass;

class AuthItemResponse
{
	public static function create(AuthItem $model): StdClass
	{
		$response = new StdClass();
		$response->name = $model->name;
		$response->category = $model->category;
		$response->role_type = $model->role_type;
		$response->description = $model->description;
		$response->name_for_user = $model->name_for_user;
		//		$response->action = BaseModel::ACTION_TYPE_CREATE;
		return $response;
	}

	public static function update(AuthItem $model, $oldName = null): StdClass
	{
		$response = new StdClass();
		$response->name = $model->name;
		$response->category = $model->category;
		$response->role_type = $model->role_type;
		$response->description = $model->description;
		$response->name_for_user = $model->name_for_user ?? $model->name;
		//		$response->action = BaseModel::ACTION_TYPE_UPDATE;
		if ($oldName) {
			$response->old_name = $oldName;
		}
		return $response;
	}
}
