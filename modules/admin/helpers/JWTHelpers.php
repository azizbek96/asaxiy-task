<?php
namespace app\modules\admin\helpers;

use app\modules\admin\entities\User\UserRefreshTokens;
use app\modules\admin\entities\User\Users;
use yii\web\ServerErrorHttpException;
use yii\base\Exception;
use Yii;

class JWTHelpers
{
    public function generateJwt(Users $user)
    {
        $jwt = Yii::$app->jwt;
        $signer = $jwt->getSigner('HS256');
        $key = $jwt->getKey();
        $time = time();

        $jwtParams = Yii::$app->params['jwt'];

        return $jwt
            ->getBuilder()
            ->issuedBy($jwtParams['issuer'])
            ->permittedFor($jwtParams['audience'])
            ->identifiedBy($jwtParams['id'], true)
            ->issuedAt($time)
            ->expiresAt($time + $jwtParams['expire'])
            ->withClaim('uid', $user->id)
            ->getToken($signer, $key);
    }

    /**
     * @throws Exception
     * @throws ServerErrorHttpException
     */
    public function generateRefreshToken(Users $user): UserRefreshTokens
    {
        $refreshToken = Yii::$app->security->generateRandomString(200);

        // TODO: Don't always regenerate - you could reuse existing one if user already has one with same IP and user agent
        $userRefreshToken = new UserRefreshTokens([
            'user_id' => $user->id,
            'token' => $refreshToken,
            'ip' => Yii::$app->request->userIP,
            'user_agent' => Yii::$app->request->userAgent,
        ]);
        if (!$userRefreshToken->save()) {
            throw new ServerErrorHttpException('Failed to save the refresh token: ' . $userRefreshToken->getErrorSummary(true));
        }

        // Send the refresh-token to the user in a HttpOnly cookie that Javascript can never read and that's limited by path
        Yii::$app->response->cookies->add(
            new \yii\web\Cookie([
                'name' => 'refresh-token',
                'value' => $refreshToken,
                'httpOnly' => true,
                'sameSite' => 'none',
                'secure' => true,
                'path' => '/v1/auth/refresh-token', //endpoint URI for renewing the JWT token using this refresh-token, or deleting refresh-token
            ])
        );

        return $userRefreshToken;
    }

}