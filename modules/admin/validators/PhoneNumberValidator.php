<?php
namespace app\modules\admin\validators;

use yii\validators\RegularExpressionValidator;

class PhoneNumberValidator extends RegularExpressionValidator
{
    public $pattern ='#^[+0-9]*$#s';
    public $message = 'Only +998999999999 symbols are allowed.';
}