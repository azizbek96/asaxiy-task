<?php

namespace app\modules\admin\entities\User;

use app\components\Constanta;
use app\modules\admin\entities\BaseModel;
use app\modules\admin\forms\auth\SignupForm;
use Yii;

/**
 * This is the model class for table "users_info".
 *
 * @property int $id
 * @property string|null $first_name
 * @property string|null $last_name
 * @property string|null $address
 * @property string|null $country_of_origin
 * @property string|null $phone_number
 * @property string|null $email
 * @property int|null $age
 * @property bool|null $hired
 * @property int|null $status
 * @property int|null $type
 * @property int|null $created_at
 * @property int|null $created_by
 * @property int|null $updated_at
 * @property int|null $updated_by
 *
 * @property Users[] $users
 */
class UsersInfo extends BaseModel
{
    public static function create(SignupForm $form): UsersInfo
    {
        $userInfo = new UsersInfo();
        $userInfo->first_name = $form->first_name;
        $userInfo->last_name = $form->last_name;
        $userInfo->address = $form->address;
        $userInfo->country_of_origin = $form->country_of_origin;
        $userInfo->phone_number = $form->phone_number;
        $userInfo->email = $form->email;
        $userInfo->status = Constanta::STATUS_ACTIVE;
        $userInfo->hired = Constanta::HIRED_FALSE;
        return $userInfo;
    }
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users_info';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['address'], 'string'],
            [['age', 'status', 'type', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'default', 'value' => null],
            [['age', 'status', 'type', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['hired'], 'boolean'],
            [['first_name', 'last_name', 'country_of_origin'], 'string', 'max' => 255],
            [['phone_number'], 'string', 'max' => 20],
            [['email'], 'string', 'max' => 60],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'first_name' => Yii::t('app', 'First Name'),
            'last_name' => Yii::t('app', 'Last Name'),
            'address' => Yii::t('app', 'Address'),
            'country_of_origin' => Yii::t('app', 'Country Of Origin'),
            'phone_number' => Yii::t('app', 'Phone Number'),
            'email' => Yii::t('app', 'Email'),
            'age' => Yii::t('app', 'Age'),
            'hired' => Yii::t('app', 'Hired'),
            'status' => Yii::t('app', 'Status'),
            'type' => Yii::t('app', 'Type'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * Gets query for [[Users]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(Users::className(), ['user_info_id' => 'id']);
    }
}
