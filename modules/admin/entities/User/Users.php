<?php

namespace app\modules\admin\entities\User;

use app\modules\admin\entities\manage\CandidateNote;
use app\modules\admin\entities\BaseModel;
use yii\base\Exception;
use yii\web\IdentityInterface;
use Yii;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property int|null $user_info_id
 * @property string|null $username
 * @property string|null $auth_key
 * @property string $password_hash
 * @property string|null $password_reset_token
 * @property int|null $status
 * @property int|null $type
 * @property int|null $created_at
 * @property int|null $created_by
 * @property int|null $updated_at
 * @property int|null $updated_by
 *
 * @property CandidateNote[] $candidateNotes
 * @property CandidateNote[] $candidateNotes0
 * @property UsersInfo $userInfo
 */
class Users extends BaseModel implements IdentityInterface
{
    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';

    const STATUS_APPLICATION = 5;// ariza berilgan
    const STATUS_TALK = 6;// interevyyu belgilangan
    const STATUS_ACCEPT = 7; //qabul qilingan
    const STATUS_NO_ACCEPT = 8; //qabul qilinmagan
    const STATUS_ADMIN = 10; // admin
    public function isActive(): bool
    {

        return $this->status !== self::STATUS_INACTIVE || $this->status !== self::STATUS_DELETED;
    }

    public static function requestSignup(string $username,  string $password): self
    {
        $user = new static();
        $user->username = $username;
        $user->setPassword($password);
        $user->status = self::STATUS_ACTIVE;
        $user->generateAuthKey();
        return $user;
    }
    public static function changeStatus(int $id,  int $status): self
    {
        $user = static::findOne(['id' => $id]);
        $user->status = $status;
        return $user;
    }


    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_info_id', 'status', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'default', 'value' => null],
            [['user_info_id', 'status', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['password_hash'], 'required'],
            [['username', 'password_hash', 'password_reset_token'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['user_info_id'], 'exist', 'skipOnError' => true, 'targetClass' => UsersInfo::className(), 'targetAttribute' => ['user_info_id' => 'id']],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_info_id' => Yii::t('app', 'User Info ID'),
            'username' => Yii::t('app', 'Username'),
            'auth_key' => Yii::t('app', 'Auth Key'),
            'password_hash' => Yii::t('app', 'Password Hash'),
            'password_reset_token' => Yii::t('app', 'Password Reset Token'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * Gets query for [[CandidateNotes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCandidateNotes()
    {
        return $this->hasMany(CandidateNote::className(), ['candidate_id' => 'id']);
    }

    /**
     * Gets query for [[CandidateNotes0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCandidateNotes0()
    {
        return $this->hasMany(CandidateNote::className(), ['user_id' => 'id']);
    }

    /**
     * Gets query for [[UserInfo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUserInfo()
    {
        return $this->hasOne(UsersInfo::className(), ['id' => 'user_info_id']);
    }

    /**
     * @param $isInsert
     * @param $changedOldAttributes
     * @return void
     */
    public function afterSave($isInsert, $changedOldAttributes)
    {
        if (array_key_exists('password_hash', $changedOldAttributes)) {
            UserRefreshTokens::deleteAll(['user_id' => $this->id]);
        }

        parent::afterSave($isInsert, $changedOldAttributes);
    }

    /**
     * @param $id
     * @return Users|IdentityInterface|null
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @param $token
     * @param $type
     * @return Users|IdentityInterface|null
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::find()
            ->andWhere(['id' => (string)$token->getClaim('uid')])
            ->andWhere(['<>', 'status', self::STATUS_INACTIVE])
            ->one();
    }

    /**
     * @param $username
     * @return Users|null
     */
    public static function findByUsername($username)
    {
        return static::findOne([
            'username' => $username,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * @param $password
     * @return bool
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * @param $password
     * @return void
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * @return void
     * @throws Exception
     * @throws Exception
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

}