<?php

namespace app\modules\admin\entities\manage;

use app\modules\admin\entities\BaseModel;
use app\modules\admin\entities\User\Users;
use Yii;

/**
 * This is the model class for table "candidate_note".
 *
 * @property int $id
 * @property int|null $candidate_id
 * @property int|null $user_id
 * @property string|null $note
 * @property string|null $date
 * @property int|null $status
 * @property int|null $type
 * @property int|null $created_at
 * @property int|null $created_by
 * @property int|null $updated_at
 * @property int|null $updated_by
 *
 * @property Users $candidate
 * @property Users $user
 */
class CandidateNote extends BaseModel
{
    public static function create($note , $date,$candidate_id = null, $user_id = null)
    {
        $candidateNote = new static();
        $candidateNote->candidate_id = $candidate_id;
        $candidateNote->user_id = $user_id;
        $candidateNote->note = $note;
        $candidateNote->date = date('Y-m-d H:i:s', strtotime($date));
        return $candidateNote;

    }
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'candidate_note';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['candidate_id', 'user_id', 'status', 'type', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'default', 'value' => null],
            [['candidate_id', 'user_id', 'status', 'type', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['note'], 'string'],
            [['date'], 'safe'],
            [['candidate_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['candidate_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'candidate_id' => Yii::t('app', 'Candidate ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'note' => Yii::t('app', 'Note'),
            'date' => Yii::t('app', 'Date'),
            'status' => Yii::t('app', 'Status'),
            'type' => Yii::t('app', 'Type'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * Gets query for [[Candidate]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCandidate()
    {
        return $this->hasOne(Users::className(), ['id' => 'candidate_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }
}
