<?php

namespace app\modules\admin\entities;

use Yii;
use yii\behaviors\TimestampBehavior;
use app\components\OurCustomBehavior;
use yii\db\ActiveRecord;

class BaseModel extends ActiveRecord
{
    const STATUS_ACTIVE = 1; // yangi intervyuga kelgan
    const STATUS_INACTIVE = 2; // faol emas
    const STATUS_SAVED = 3;
    const STATUS_DELETED = 4; // o'chirilgan

	const CREATE = 1;
	const UPDATE = 2;
	const VIEW = 3;
	const DELETE = 4;

	/**
	 * @return array
	 */
	public function behaviors()
	{
		return [
			[
				'class' => OurCustomBehavior::class,
				'updatedByAttribute' => 'updated_by',
			],
			[
				'class' => TimestampBehavior::class,
			],
		];
	}

	/**
	 * @param $key
	 * @return array|mixed
	 */
	public static function getStatusList($key = null)
	{
		$result = [
			self::STATUS_DELETED => Yii::t('app', 'Deleted'),
			self::STATUS_ACTIVE => Yii::t('app', 'Active'),
			self::STATUS_INACTIVE => Yii::t('app', 'Inactive'),
			self::STATUS_SAVED => Yii::t('app', 'Saved'),
		];
		if (!is_null($key)) {
			return $result[$key];
		}
		return $result;
	}
}
