<?php

namespace app\modules\admin\services\auth;


use app\modules\admin\entities\User\UsersInfo;
use app\modules\admin\forms\auth\SignupForm;
use app\modules\admin\entities\User\Users;
use app\modules\admin\repositories\SignupRepository;
use app\modules\admin\repositories\UsersRepository;
use Yii;

class SingupService
{
    private $user;
    private $signup;

    public function __construct(
        UsersRepository $user,
        SignupRepository $signup
    )
    {
        $this->user = $user;
        $this->signup = $signup;
    }

    /**
     * @throws \Exception
     */
    public function signup(SignupForm $form): ?Users
    {

        $userInfo =  UsersInfo::create($form);
        $user = Users::requestSignup(
            $form->username,
            $form->password_hash
        );
        $transaction =  Yii::$app->db->beginTransaction();
        try {
            $saved = false;
            if ($this->signup->save($userInfo)){
                $user->user_info_id = $userInfo->id;
                if ($this->user->save($user)){
                    $saved= true;
                }
            }
            if ($saved){
                $transaction->commit();
                return $user;
            }else{
                $transaction->rollBack();
            }
        }catch (\Exception $e){
            $transaction->rollBack();
        }
        return null;
    }

    private function save(Users $user)
    {
        if (!$user->save()) {
            throw new \RuntimeException('Saving error.');
        }
    }

}