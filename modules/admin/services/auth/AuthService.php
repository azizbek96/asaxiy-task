<?php

namespace app\modules\admin\services\auth;

use app\modules\admin\entities\User\Users;
use app\modules\admin\forms\auth\LoginForm;
use app\modules\admin\repositories\UsersRepository;

class AuthService
{
    private $users;

    public function __construct(UsersRepository $users)
    {
        $this->users = $users;
    }

    public function auth(LoginForm $form):Users
    {
        $user = $this->users->findByUsername($form->username);
        if (!$user || !$user->isActive() || !$user->validatePassword($form->password)) {
            throw new \DomainException('Undefined user or password.');
        }
        return $user;
    }
}