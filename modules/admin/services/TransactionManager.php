<?php

namespace app\modules\admin\service;

class TransactionManager
{
    /**
     * @throws \Exception
     */
    public function wrap(callable $function)
    {
        \Yii::$app->db->transaction($function);
    }
}