<?php

namespace app\modules\admin\forms;

use app\modules\admin\entities\User\Users;
use yii\base\Model;
/**
 * This is the model class for table "candidate_note".
 *
 * @property int $id
 * @property int|null $candidate_id
 * @property int|null $user_id
 * @property string|null $note
 * @property string|null $date
 * @property int|null $status
 * @property int|null $type
 *
 * @property Users $candidate
 * @property Users $user
 */
class CandidateNoteForm extends Model
{
    public $id;
    public $candidate_id;
    public $user_id;
    public $note;
    public $date;
    public $status;
    public $type;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['candidate_id', 'date', 'note',], 'required',],
            [['candidate_id', 'user_id'], 'integer'],
            [['note'], 'string'],
            [['date'], 'safe'],
            [['candidate_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['candidate_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }
}