<?php

namespace app\modules\admin\forms;

use app\modules\admin\entities\User\Users;
use yii\base\Model;

/**
 * This is the model class for table "candidate_note".
 *
 * @property int $candidate_id
 * @property int $status
 * @property bool $hired
 */
class CandidateStatusForm extends Model
{
    public $candidate_id;
    public $status;
    public $hired;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['candidate_id', 'status',], 'required',],
            [['candidate_id', 'status'], 'integer'],
            ['hired', 'boolean'],
            [['candidate_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['candidate_id' => 'id']],
        ];
    }
}