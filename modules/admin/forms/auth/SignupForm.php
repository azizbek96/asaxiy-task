<?php

namespace app\modules\admin\forms\auth;

use app\modules\admin\validators\PhoneNumberValidator;
use yii\base\Model;
use Yii;

/**
 *
 *
 * @property string|null $username
 * @property string|null $password_hash
 *
 * Signup form
 *
 * This is the model class for table "users_info".
 *
 * @property int $id
 * @property string|null $first_name
 * @property string|null $last_name
 * @property string|null $address
 * @property string|null $country_of_origin
 * @property string|null $phone_number
 * @property string|null $email
 * @property int|null $age
 * @property bool|null $hired
 */
class SignupForm extends Model
{
    public $username;
    public $password_hash;

    public $first_name;
    public $last_name;
    public $address;
    public $country_of_origin;
    public $phone_number;
    public $email;
    public $age;
    public $hired;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => 'app\modules\admin\entities\User\Users', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 3, 'max' => 50],

            ['password_hash', 'required'],
            ['password_hash', 'string', 'min' => 8 ],
            [['first_name', 'last_name', 'address', 'country_of_origin', 'email', 'phone_number', 'age',], 'required'],
            [['first_name', 'last_name',], 'string', 'min' => 5, 'max' => 255],
            [['address'], 'string', 'min' => 10],
            [['country_of_origin'], 'string'],
            [['email'], 'email'],
            [['email'], 'string', 'max' => 60],
            [ 'email', 'unique', 'targetClass' => 'app\modules\admin\entities\User\UsersInfo',
                'message' => 'This email address has already been taken.'
            ],

            [['phone_number'], 'string', 'max' => 20],
            ['phone_number', PhoneNumberValidator::class],

            [['age'], 'integer','min' => 1, 'max' => 150],
            [['hired'], 'boolean'],
        ];
    }

}
