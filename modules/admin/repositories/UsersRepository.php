<?php

namespace app\modules\admin\repositories;

use app\modules\admin\models\AuthAssignment;
use app\modules\admin\entities\User\Users;
use app\modules\api\models\BaseModel;
use app\components\Constanta;
use Yii;

class UsersRepository
{

    public function findByUsername($value)
    {
        return Users::findOne(['username' => $value]);
    }

    public function save(Users $user){
        if (!$user->save()){
            throw new \RuntimeException('Saving error');
        }
        return true;
    }
	public static function getList(): array
    {
		return Users::find()
			->select([
				'value' => 'id',
				'label' => 'username',
			])
			->where(['status' => Constanta::STATUS_ACTIVE])
			->asArray()
			->all();
	}

    private function getBy(array $condition):Users
    {
        if (!$user = Users::find()->andWhere($condition)->limit(1)->one()){
            throw new NotFoundException('User not found.');
        }
        return $user;
    }
    public function aplication($id)
    {
        $user = $this->getBy(['id' => $id, 'status' => BaseModel::STATUS_ACTIVE]);
        $user->type = Users::STATUS_APPLICATION;
        $this->save($user);
    }
    public function message($id)
    {
        $user = $this->getBy(['id' => $id, 'status' => BaseModel::STATUS_ACTIVE]);
        $user->type = Users::STATUS_TALK;
        $this->save($user);
    }
	/**
	 * @param $data
	 * @param $id
	 * @return array|bool[]
	 */
	public static function saveModel($data, $id = null): array
	{
		$transaction = Yii::$app->db->beginTransaction();
		try {
			if ($id == null) {
				/**
				 * @var Users $model
				 * Yangi user yaratish
				 */
				$model = new Users();
				$model->scenario = Users::SCENARIO_CREATE;
				if ($data['password'] === $data['confirm_password']) {
					$model->setAttributes([
						'username' => $data['username'],
						'status' => BaseModel::STATUS_ACTIVE,
						'auth_key' => Yii::$app->security->generateRandomString(),
						'password_hash' => Yii::$app->security->generatePasswordHash($data['password']),
					]);
				} else {
					$model->addError('confirm_password', 'Password and Confirm Password not match');
					$transaction->rollBack();
					return [
						'status' => false,
						'errors' => $model->errors,
					];
				}
			} else {
				/**
				 * @var $model Users
				 * foydalanuvchi update qilish
				 */
				$model = Users::findOne($id);
				$model->scenario = Users::SCENARIO_UPDATE;
				$model->setAttributes([
					'username' => $data['username'],
					'status' => BaseModel::STATUS_ACTIVE,
				]);
			}

			if ($model->save()) {
				/**
				 * @var AuthAssignment $authAssignment
				 * Role biriktirish
				 */
				$roles = $data['roles'];
				if (!empty($roles)) {
					AuthAssignment::deleteAll(['user_id' => $model->id]);
					foreach ($roles as $key => $role) {
						$authAssignment = new AuthAssignment([
							'item_name' => $key,
							'user_id' => (string) $model->id,
							'created_at' => time(),
						]);
						if (!$authAssignment->save()) {
							$transaction->rollBack();
							return [
								'status' => false,
								'message' => Yii::t('app', 'Role saqlanmadi!'),
								'errors' => $authAssignment->getErrors(),
							];
						}
					}
				}
				$transaction->commit();
				return [
					'status' => true,
					'id' => $model->id,
				];
			} else {
				$transaction->rollBack();
				return [
					'status' => false,
					'errors' => $model->errors,
				];
			}
		} catch (\Exception $e) {
			$transaction->rollBack();
			return [
				'status' => false,
				'errors' => $e->getMessage(),
			];
		}
	}

	/**
	 * @param $id
	 * @return array
	 */
	public static function getUser($id)
	{
		$models = Users::findOne(['id' => $id]);
		return [
			'id' => $models->id,
			'status' => $models->status,
			'username' => $models->username,
			'hr_employee_id' => $models->hrEmployee->first_name . ' ' . $models->hrEmployee->last_name . ' ' . $models->hrEmployee->father_name,
		];
	}
}
