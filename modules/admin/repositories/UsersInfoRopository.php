<?php

namespace app\modules\admin\repositories;

use app\modules\admin\entities\BaseModel;
use app\modules\admin\entities\User\UsersInfo;
use app\modules\admin\models\Users;
use yii\db\Exception;

class UsersInfoRopository
{
    public function getUserInfo($id)
    {
        $userInfo = Users::find()->alias('u')
            ->select([
                "ui.first_name",
                "ui.last_name",
                "ui.address",
                "ui.country_of_origin",
                "ui.phone_number",
                "ui.email",
                "ui.age",
                "u.status",
            ])
            ->leftJoin(['ui' => UsersInfo::find()],'ui.id = u.user_info_id')
            ->where(['u.status' => BaseModel::STATUS_ACTIVE])
            ->andWhere(['u.id' => $id])
            ->asArray()->limit(1)->one();
            if (!empty($userInfo)){
                return ['data' => $userInfo, 'status' => true];
            }
            return ['data' => '', 'status' => false];;
    }
    public static function getUserInfoModel($id)
    {
        if (!$userInfo = UsersInfo::findOne($id)){
            throw new \DomainException('Error');
        }
        return $userInfo;
    }
}