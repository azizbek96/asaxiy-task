<?php

namespace app\modules\admin\repositories;

use app\modules\admin\entities\User\UsersInfo;

class SignupRepository
{
    public function save(UsersInfo $usersInfo)
    {
        if (!$usersInfo->save()){
            throw new \RuntimeException('Saving error');
        }
        return true;
    }

    public function remove(UsersInfo $usersInfo)
    {
        if (!$usersInfo->delete()) {
            throw new \RuntimeException('Removing error');
        }
    }
}