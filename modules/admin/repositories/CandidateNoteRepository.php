<?php

namespace app\modules\admin\repositories;

use app\modules\admin\entities\manage\CandidateNote;

class CandidateNoteRepository
{
    public function save(CandidateNote $candidateNote)
    {
        if (!$candidateNote->save()){
            throw new \RuntimeException('Saving error');
        }
        return true;
    }

    public static function get($id)
    {
        if (!$candidate = CandidateNote::findOne(['candidate_id' => $id])) {
            return false;
        }
        return $candidate;
    }
}