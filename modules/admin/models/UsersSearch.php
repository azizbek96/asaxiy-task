<?php

namespace app\modules\admin\models;

use yii\data\SqlDataProvider;
use app\modules\hrm\models\HrEmployees;

class UsersSearch extends Users
{
	public function rules()
	{
		return [[['id', 'status', 'created_at', 'updated_at'], 'integer'], [['username', 'email', 'password_hash', 'auth_key', 'password_reset_token'], 'safe']];
	}

	/**
	 * @param $params
	 * @param int $page
	 * @return SqlDataProvider
	 */
	public function search($params, int $page = 1): SqlDataProvider
	{
		$query = Users::find()->alias('u');

		$query->select([
            'u.id',
            'u.status',
            'u.username',
            'hr_employee_id' => "CONCAT(he.first_name, ' ', he.last_name,' ', he.father_name)"
        ]);
		$query
            ->where(['!=', 'u.status', BaseModel::STATUS_DELETED])
            ->orderBy('u.username ASC');

		$query = $query->leftJoin(['he' => HrEmployees::tableName()], 'he.id = u.hr_employee_id');

		$this->load($params, '');


		$query->andFilterWhere([
			'u.id' => $this->id,
			'u.status' => $this->status,
			'u.created_at' => $this->created_at,
			'u.updated_at' => $this->updated_at,
		]);
		$query->andFilterWhere(['like', 'u.username', $this->username]);


		$dataProvider = new SqlDataProvider([
			'sql' => $query->createCommand()->getRawSql(),
			'sort' => [
				'attributes' => [
					'u.username' => [
						'asc' => ['u.username' => SORT_ASC],
						'desc' => ['u.username' => SORT_DESC],
						'default' => SORT_ASC,
					]
				],
			],
			'pagination' => [
				'pageSize' => 20,
				'page' => $page - 1,
			],
		]);
		
		if (!$this->validate()) {
			return $dataProvider;
		}

		return $dataProvider;
	}
}