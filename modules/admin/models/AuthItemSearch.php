<?php

namespace app\modules\admin\models;

use yii\base\Model;
use yii\data\SqlDataProvider;
use yii\data\ActiveDataProvider;
use app\modules\api\models\BaseModel;

/**
 * AuthItemsSearch represents the model behind the search form of `app\modules\admin\models\AuthItem`.
 */
class AuthItemSearch extends AuthItem
{
	public bool $isRole = true;
	public bool $isPermission = false;

	/**
	 * {@inheritdoc}
	 */
	public function rules()
	{
		return [
			[['name', 'description', 'rule_name', 'data', 'category', 'name_for_user'], 'safe'],
			[['type', 'created_at', 'updated_at', 'role_type'], 'integer'],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function scenarios()
	{
		return Model::scenarios();
	}

	public function permission($permission = true): AuthItemSearch
	{
		$this->isRole = !$permission;
		$this->isPermission = $permission;

		return $this;
	}

	public function role($role = true): AuthItemSearch
	{
		$this->isRole = $role;
		$this->isPermission = !$role;

		return $this;
	}

	/**
	 * @param $params
	 * @return SqlDataProvider
	 */
	public function search($params, $page = 1)
	{
		$query = AuthItem::find()
			->select(['name', 'category', 'role_type', 'description', 'name_for_user'])
			->orderBy(['created_at' => SORT_DESC]);

		$query->where([
			'type' => $this->isRole ? BaseModel::USER_ROLE_TYPE_ROLE : BaseModel::USER_ROLE_TYPE_PERMISSION,
		]);


		$query->andFilterWhere([
			'type' => $this->type,
			'role_type' => $this->role_type,
			'created_at' => $this->created_at,
			'updated_at' => $this->updated_at,
		]);

		$query
			->andFilterWhere(['ilike', 'name', $this->name])
			->andFilterWhere(['ilike', 'description', $this->description])
			->andFilterWhere(['ilike', 'name_for_user', $this->name_for_user])
			->andFilterWhere(['ilike', 'rule_name', $this->rule_name])
			->andFilterWhere(['ilike', 'data', $this->data]);

		$dataProvider = new SqlDataProvider([
			'sql' => $query->createCommand()->rawSql,
			'pagination' => [
				'pageSize' => 10,
				'page' => $page - 1,
			],
		]);

		$this->load($params, '');

		if (!$this->validate()) {
			return $dataProvider;
		}

		return $dataProvider;
	}
}
