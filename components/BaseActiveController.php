<?php

namespace app\components;

use app\modules\admin\models\Users;
use app\modules\api\models\BaseModel;
use Lcobucci\JWT\Parser;
use sizeg\jwt\JwtHttpBearerAuth;
use Yii;
use yii\rest\ActiveController;
use yii\web\BadRequestHttpException;

class BaseActiveController extends ActiveController
{
	/**
	 * @return array
	 */
	public function behaviors(): array
	{
		$behaviors = parent::behaviors();

		$auth = $behaviors['authenticator'] = [
			'class' => JwtHttpBearerAuth::class,
		];

		unset($behaviors['authenticator']);

		$behaviors['corsFilter'] = [
			'class' => CorsCustom::class,
		];

		$behaviors['authenticator'] = $auth;

		$behaviors['authenticator']['except'] = ['login', 'refresh-token', 'options'];

		return $behaviors;
	}

	/**
	 * @param $action
	 * @return bool
	 * @throws BadRequestHttpException
	 */
	public function beforeAction($action): bool
	{
		$this->enableCsrfValidation = false;
		$headers = Yii::$app->request->headers;
		$pattern = "/^Bearer\s+(.*?)$/";
		preg_match($pattern, $headers->get('authorization'), $matches);
		if (isset($matches[1])) {
			$token = (new Parser())->parse($matches[1]);
			$userId = $token->getClaim('uid');
			if ($userId) {
				$user = Users::findOne(['id' => $userId]);
				if ($user !== null) {
					Yii::$app->user->setIdentity($user);
				}
			}
		}
		return parent::beforeAction($action);
	}

	/**
	 * @param mixed|null $data
	 * @param string $message
	 * @return array
	 */
	public function success($data = [], string $message = 'Success'): array
	{
		return [
			'data' => $data,
			'message' => $message,
			'code' => BaseModel::CODE_SUCCESS,
		];
	}

	/**
	 * @param array $data
	 * @param string $message
	 * @param int $code
	 * @return array
	 */
	public function error(array $data = [], string $message = 'Error', int $code = BaseModel::CODE_ERROR): array
	{
		if ($message == null) {
			$message = 'Xatolik yuz berdi!';
		}
		return [
			'data' => $data,
			'code' => $code,
			'message' => $message,
		];
	}
}
