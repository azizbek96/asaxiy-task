<?php

namespace app\components;

class Constanta
{
	/**
	 * @param int [hr_employees of type column]
	 */
	const TYPE_EMPLOYEE_REGISTRATION = 1; //ro'yhatga olingan hodimlar
	const TYPE_EMPLOYEE_WORKING = 2; //ishlayotgan hodimlar
	const TYPE_EMPLOYEE_DISMISSAL = 3; //bo'shatilgan hodimlar

	/** end  [hr_employees of type column] */

	//    ----------------

    const STATUS_ACTIVE = 1; // yangi intervyuga kelgan
    const STATUS_INACTIVE = 2; // faol emas
    const STATUS_SAVED = 3;
    const STATUS_DELETED = 4; // o'chirilgan
    const STATUS_APPLICATION = 5;// ariza berilgan
    const STATUS_TALK = 6;// interevyyu belgilangan
    const STATUS_ACCEPT = 7; //qabul qilingan
    const STATUS_NO_ACCEPT = 8; //qabul qilinmagan
    const STATUS_ADMIN = 10; // admin

    const HIRED_FALSE = false; //ishga olinmagan
    const HIRED_TRUE = true;//ishga olingan
	/** @var int * [hr_employees_rel_attachments of type column] */

	const TYPE_IMAGE_AVATAR = 1; //main image

	/** end [hr_employees_rel_attachments] */

}
