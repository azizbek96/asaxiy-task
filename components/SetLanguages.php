<?php

namespace app\components;

use Yii;
use yii\base\Application;
use yii\base\Behavior;

class SetLanguages extends Behavior
{
    public function events(): array
    {
        return [
            Application::EVENT_BEFORE_REQUEST => 'beforeRequest',
        ];
    }

    public function beforeRequest()
    {
        if (isset(Yii::$app->request->bodyParams['lang'])) {

            $lang = Yii::$app->request->bodyParams['lang'];

            Yii::$app->session->set('lang', $lang);
            Yii::$app->language = $lang;

        } elseif (Yii::$app->session->has('lang')) {

            $lang = Yii::$app->session->get('lang');

            if ($lang) {
                Yii::$app->language = $lang;
            }
        }
    }
}